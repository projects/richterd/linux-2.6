/*
 *  linux/fs/gfs2/pnfs_gfs2_types.h
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#ifndef _PNFS_GFS2_TYPES_H_
#define _PNFS_GFS2_TYPES_H_

/*
 * This file is meant to be used by in-kernel gfs2 to be compatible
 * with the gfs2 userland and with nfs-related types.
 *
 * A similar file of the same name exists in userland; unlike this one,
 * however, the userland copy also directly specifies definitions of
 * various nfs-related types (e.g., stateids).
 */

#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/completion.h>
#include <linux/buffer_head.h>
#include <linux/exportfs.h>
#include <linux/crc32.h>
#include <linux/lm_interface.h>

#include <linux/completion.h>
#include <linux/mount.h>
#include <linux/nfs_fs.h>
#include <linux/sunrpc/svc.h>
#include <linux/nfsd/state.h>
#include <linux/nfsd/nfsfh.h>
#include <linux/nfsd/pnfsd.h>
#include <linux/nfsd/nfs4layoutxdr.h>
#include <linux/nfs4_pnfs.h>


/*
 * Message header format used in pNFS-related GFS2 messages.  When sent over
 * one of the existing GFS channels we use -- (which are:
 *   (a) the multicast-based gfs_controld<->gfs_controld channel, and
 *   (b) the unix-socket-based gfs_control<->gfs_controld channel.
 * ) -- pNFS msgs (struct pipefs_hdr's) are wrapped by the appropriate GFS header.
 *
 * A new, third GFS-related channel is introduced for pNFS and uses basically
 * the same messages without an initial GFS-specific header:
 *   (c) the rpc_pipefs-based gfs_controld<->gfs-kernel channel
 *
 * Note that we use (c) above instead of GFS's existing kernel<->userland
 * communication methods, which are too limited for our purposes (upcalls are
 * made by broadcasting uevents from the kernel over a netlink socket;
 * "downcalls" aren't really downcalls -- it's just individual sysfs variables
 * getting twiddled).
 *
 * As a message example, commands on channel (a) above use a struct gfs_header
 * (that is, not struct gfsc_header like (b) uses), so a "get_state" request
 * from a remote gfs_controld looks like:
 *
 * ----------------------------------        (see userland cpg-new.c)
 * | struct gfs_header              | .type = GFS_MSG_PNFS
 * |--------------------------------|
 * || struct pipefs_hdr            || .type = PNFS_GET_STATE_REQ
 * ||------------------------------||
 * ||| struct gfs2_pnfs_get_state |||
 * |||----------------------------|||
 * ----------------------------------
 *
 *
 * Commands on channel (b) above use a struct gfsc_header, and so look like:
 *
 * ----------------------------------          (see userland gfs_controld.h)
 * | struct gfsc_header             | .option = GFSC_CMD_PNFS
 * |--------------------------------|
 * || struct pipefs_hdr            || .type = PNFS_GET_STATE_REQ
 * ||------------------------------||
 * ||| struct gfs2_pnfs_get_state |||
 * |||----------------------------|||
 * ----------------------------------
 *
 */

#define IS_REQ(type)	(type == PNFS_GET_STATE_REQ || \
			 type == PNFS_GET_VERIFIER_REQ)
#define IS_RES(type)	(type == PNFS_GET_STATE_RES || \
			 type == PNFS_GET_VERIFIER_RES)
enum {
	PNFS_INVAL = 0,
	PNFS_GFS2_SET_NODEID = 1,
	PNFS_GFS2_SET_DS_LIST = 2,
	PNFS_GET_STATE_REQ = 3,
	PNFS_GET_STATE_RES = 4,
	PNFS_CHANGE_STATE_NOTIF = 5,
	PNFS_GET_VERIFIER_REQ = 6,
	PNFS_GET_VERIFIER_RES = 7,
};

struct gfs2_set_nodeid {
	u32 our_nodeid;
	u32 mds_nodeid;
	u32 groupid;
};

struct gfs2_ds_info {
	u32 nodeid;
	char ip_quad[16];	/* up to 12 digits + 3 decimals + NULL-term */
};

struct gfs2_ds_list {
	int num_dses;
	struct gfs2_ds_info dses[0];
};

struct gfs2_pnfs_device {
	struct gfs2_ds_info ds_info;
	struct list_head all_layouts;
	struct list_head all_devices;
};

struct gfs2_pnfs_get_state {
	struct pnfs_get_state pnfs_get_state;
	u32 groupid;
};

struct gfs2_pnfs_get_verifier {
	nfs4_verifier verifier;
	u32 groupid;
};

struct gfs2_layout_info {
	struct nfsd4_layout_seg layout_seg;
	struct list_head per_sb;
	struct list_head per_inode;
};

#endif /* _PNFS_GFS2_TYPES_H_ */
