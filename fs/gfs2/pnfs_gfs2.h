/*
 *  linux/fs/gfs2/pnfs_gfs2.h
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#ifndef _PNFS_GFS2_H_
#define _PNFS_GFS2_H_

#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/sunrpc/svc.h>
#include <linux/nfs3.h>
#include <linux/nfsd/nfsfh.h>
#include <linux/nfsd/state.h>
#include <linux/nfsd/pnfsd.h>
#include <linux/nfsd/nfsd.h>
#include <linux/sunrpc/simple_rpc_pipefs.h>
#include <linux/gfs2_ondisk.h>

#include "pnfs_gfs2_types.h"
#include "pnfs_gfs2_glue.h"
#include "incore.h"
#include "ops_fstype.h"

/*
 * The pipe appears in the root of the mounted rpc_pipefs filesystem;
 * generally, this means in /var/lib/nfs/rpc_pipefs/.
 */
#define XXX_PNFS_GFS2_PIPEFS_FILE     "pnfs-gfs2"
#define XXX_PNFS_GFS2_PIPEFS_MAXSZ    4096


/* pnfs_gfs2_sysctl.c */
extern int gfs2_register_sysctl(void);
extern void gfs2_unregister_sysctl(void);

/* number of characters in the comma-delimited ds_list string of dotted quads */
#define XXX_PNFS_DS_LISTSZ 256
extern char pnfs_ds_list[];

/* pnfs_gfs2.c */
u32* gfs2_verifier(void);

extern struct list_head gfs2_pnfs_devices;
extern spinlock_t gfs2_pnfs_device_lock;

/* pnfs_gfs2_pipefs.c */
int gfs2_init_pnfs_pipe(void);
void gfs2_shutdown_pnfs_pipe(void);
struct pipefs_hdr* alloc_init_msg(u8, u8, void *, u16);
struct pipefs_hdr* alloc_init_msg_padded(u8, u8, void *, u16, u16);
struct pipefs_hdr* queue_upcall_waitreply(struct pipefs_hdr *, u8);
int queue_upcall_noreply(struct pipefs_hdr *, u8);


#endif /* _PNFS_GFS2_H_ */
