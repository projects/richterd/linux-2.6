/*
 *  fs/gfs2/gfs2_pnfs_glue.h 
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#ifndef _GFS2_PNFS_GLUE_H_
#define _GFS2_PNFS_GLUE_H_

/*
 * This file is meant to provide the GFS2 parts with enough information
 * about the rest of the pNFS parts.
 */

#include <linux/exportfs.h>
#include <linux/fs.h>

/* for the sake of the stateid_t addition to GFS2's "incore.h" */
#if defined(CONFIG_PNFSD)
#include <linux/sunrpc/svc.h>
#include <linux/nfs3.h>
#include <linux/nfsd/state.h>
#endif /* CONFIG_PNFSD */

/* pnfs_gfs2.c */
#if defined(CONFIG_PNFSD)
struct gfs2_sbd;

extern int gfs2_init_pnfs(void);
extern void gfs2_shutdown_pnfs(void);
extern void gfs2_set_pnfs_groupid(struct gfs2_sbd *, const char *);
#else /* !CONFIG_PNFSD */
#define gfs2_init_pnfs()			0
#define gfs2_shutdown_pnfs()			do { } while (0)
#define gfs2_set_pnfs_groupid(sd, s)		do { } while (0)
#endif /* CONFIG_PNFSD */

/* pnfs_gfs2.c */
#if defined(CONFIG_PNFSD)
int gfs2_layout_type(struct super_block *);
int gfs2_layout_get(struct inode *, struct pnfs_layoutget_arg *);
int gfs2_layout_return(struct inode *, void *);
int gfs2_get_device_iter(struct super_block *, struct pnfs_deviter_arg *);
int gfs2_get_device_info(struct super_block *, struct pnfs_devinfo_arg *);
int gfs2_get_state(struct inode *, void *, void *);
void gfs2_change_state(struct file *);
void gfs2_get_verifier(struct super_block *, u32 *);
void gfs2_layout_recall_file(struct dentry *);
#endif /* CONFIG_PNFSD */


#endif /* _GFS2_PNFS_GLUE_H_ */
