/*
 *  fs/gfs2/pnfs_gfs2_sysctl.c 
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

/*
 * XXX: the /proc interface for specifying pNFS DSes and possibly other
 *  stuff in the future.  this should be pruned, replaced, or moved.
 */

#include <linux/sysctl.h>
#include <linux/fs.h>

#include "pnfs_gfs2.h"


char pnfs_ds_list[XXX_PNFS_DS_LISTSZ];

static struct ctl_table_header *gfs2_sysctl_table;

static ctl_table gfs2_sysctls[] = {
	{
		.ctl_name	= CTL_UNNUMBERED,
		.procname	= "pnfs_ds_list",
		.data		= &pnfs_ds_list,
		.maxlen		= XXX_PNFS_DS_LISTSZ,
		.mode		= 0644,
		.proc_handler	= &proc_dostring,
		.strategy	= &sysctl_string,
	},
	{	.ctl_name	= 0 }
};

static ctl_table gfs2_sysctl_dir[] = {
	{
		.ctl_name	= CTL_UNNUMBERED,
		.procname	= "gfs2",
		.mode		= 0555,
		.child		= gfs2_sysctls,
	},
	{	.ctl_name	= 0 }
};

static ctl_table gfs2_sysctl_root[] = {
	{
		.ctl_name	= CTL_FS,
		.procname	= "fs",
		.mode		= 0555,
		.child		= gfs2_sysctl_dir,
	},
	{	.ctl_name	= 0 }
};

int gfs2_register_sysctl(void)
{
	gfs2_sysctl_table = register_sysctl_table(gfs2_sysctl_root);
	if (gfs2_sysctl_table == NULL)
		return -ENOMEM;
	return 0;
}

void gfs2_unregister_sysctl(void)
{
	unregister_sysctl_table(gfs2_sysctl_table);
	gfs2_sysctl_table = NULL;
}

