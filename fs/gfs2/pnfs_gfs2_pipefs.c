/*
 *  linux/fs/gfs2/pnfs_gfs2_pipefs.c
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#include "pnfs_gfs2.h"

static ssize_t gfs2_pnfs_downcall(struct file *, const char __user *, size_t);
static struct rpc_pipe_ops gfs2_pnfs_pipefs_ops = {
	.upcall		= pipefs_generic_upcall,
	.destroy_msg	= pipefs_generic_destroy_msg,
	.downcall	= gfs2_pnfs_downcall,
};

static struct dentry *gfs2_pnfs_pipe;
static struct pipefs_list upcall_list;

static spinlock_t gfs2_XXX_msgid_lock;
static u32 gfs2_XXX_msgid;


int gfs2_init_pnfs_pipe(void)
{
	gfs2_pnfs_pipe = pipefs_mkpipe(XXX_PNFS_GFS2_PIPEFS_FILE,
				       &gfs2_pnfs_pipefs_ops, 1);
	if (IS_ERR(gfs2_pnfs_pipe)) {
		printk(KERN_ERR "%s: ERROR: couldn't make pipe (%ld)\n",
			__func__, PTR_ERR(gfs2_pnfs_pipe));
		return PTR_ERR(gfs2_pnfs_pipe);
	}

	pipefs_init_list(&upcall_list);
	spin_lock_init(&gfs2_XXX_msgid_lock);
	return 0;
}

void gfs2_shutdown_pnfs_pipe(void)
{
	pipefs_closepipe(gfs2_pnfs_pipe);
}

static u32 get_next_msgid(void)
{
	u32 msgid;

	spin_lock(&gfs2_XXX_msgid_lock);
	msgid = gfs2_XXX_msgid++;
	spin_unlock(&gfs2_XXX_msgid_lock);
	return msgid;
}

struct pipefs_hdr* alloc_init_msg(u8 type, u8 flags, void *data, u16 datalen)
{
	return pipefs_alloc_init_msg(get_next_msgid(), type, flags, data,
				     datalen);
}

struct pipefs_hdr* alloc_init_msg_padded(u8 type, u8 flags, void *data,
					 u16 datalen, u16 padlen)
{
	return pipefs_alloc_init_msg_padded(get_next_msgid(), type, flags,
					    data, datalen, padlen);
}

struct pipefs_hdr* queue_upcall_waitreply(struct pipefs_hdr *msg, u8 upflags)
{
	return pipefs_queue_upcall_waitreply(gfs2_pnfs_pipe, msg, &upcall_list,
					     upflags, 0);
}

int queue_upcall_noreply(struct pipefs_hdr *msg, u8 upflags)
{
	return pipefs_queue_upcall_noreply(gfs2_pnfs_pipe, msg, upflags);
}

static inline int assign_upcall_reply(struct pipefs_hdr *msg)
{
	return pipefs_assign_upcall_reply(msg, &upcall_list);
}

/*
 * if @get_fs_sb is nonzero, a struct gfs2_sbd* is returned; otherwise, the
 * corresponding struct super_block* is returned;
 */
static void* __gfs2_find_sbish_groupid(u32 groupid, int get_fs_sb)
{
	struct super_block *sb;
	struct gfs2_sbd *gsb;

	list_for_each_entry(sb, &gfs2_fs_type.fs_supers, s_instances) {
		gsb = GFS2_SBD(sb);
		if (gsb->groupid == groupid)
			return get_fs_sb ? (void *)gsb : (void *)sb;
	}
	return NULL;
}

static struct super_block* gfs2_find_sb_groupid(u32 groupid)
{
	return (struct super_block *)__gfs2_find_sbish_groupid(groupid, 0);
}

static struct gfs2_sbd* gfs2_find_sbd_groupid(u32 groupid)
{
	return (struct gfs2_sbd *)__gfs2_find_sbish_groupid(groupid, 1);
}

static int handle_get_state_req(struct pipefs_hdr *msg)
{
	int err = 0;
	struct pnfs_cb_get_state cgs;
	struct gfs2_pnfs_get_state *ggs;
	struct gfs2_file *gfp;
	struct super_block *sb;
	u32 *vin, *vout;

	ggs = payload_of(msg);

	printk("%s: DOWNCALL GET_STATE_REQ for " STATEID_FMT "\n", __func__,
		STATEID_VAL(&ggs->pnfs_get_state.stid));

	sb = gfs2_find_sb_groupid(ggs->groupid);
	if (!sb) {
		err = -ENOENT;
		goto out;
	}
	if (!sb->s_export_op->cb_get_state) {
		printk(KERN_ERR "%s: ERROR -- cb_get_state unset\n", __func__);
		err = -ENOTSUPP;
		goto out;
	}
	cgs.get_state = &ggs->pnfs_get_state;
	cgs.vfs_file = NULL;

	msg->type = PNFS_GET_STATE_RES;

	printk(KERN_ERR "%s: DMR: why do i have to print cb_get_state's ptr to avoid an oops?? %p\n", __func__, sb->s_export_op->cb_get_state);

	msg->status = sb->s_export_op->cb_get_state(sb, &cgs);

	if (!msg->status) {
		if (cgs.vfs_file == NULL) {
			printk(KERN_ERR "%s: ERROR: NULL vfs_file\n", __func__);
			err = -EBADFD;
			goto out;
		}

		/* on success, send back verifier */
		vin = gfs2_verifier();
		vout = (u32 *)&cgs.get_state->verifier;
		*vout++ = *vin++;
		*vout = *vin;

		/* store the stateid so that we can revoke it later */
		gfp = (struct gfs2_file *)cgs.vfs_file->private_data;
		memcpy(&gfp->pnfs_stateid, &ggs->pnfs_get_state.stid,
			sizeof(ggs->pnfs_get_state.stid));
		printk(KERN_ERR "%s: OKAY, stashed stateid " STATEID_FMT
			"\n", __func__, STATEID_VAL(&gfp->pnfs_stateid));
	}
out:
	return err;
}

static void handle_change_state_notif(struct pipefs_hdr *msg)
{
	int err = 0;
	struct gfs2_pnfs_get_state *ggs;
	struct super_block *sb;

	ggs = payload_of(msg);
	printk("%s: DOWNCALL CHANGE_STATE_NOTIF for " STATEID_FMT "\n",
		__func__, STATEID_VAL(&ggs->pnfs_get_state.stid));

	sb = gfs2_find_sb_groupid(ggs->groupid);
	if (!sb) {
		printk(KERN_ERR "%s: ERROR! couldn't find sb for groupid %u\n",
			__func__, ggs->groupid);
		goto out;
	}
	if (!sb->s_export_op->cb_change_state) {
		printk(KERN_ERR "%s: ERROR: cb_change_state unset\n", __func__);
		goto out;
	}

	err = sb->s_export_op->cb_change_state(&ggs->pnfs_get_state);
	printk("%s: cb_change_state returned %d\n", __func__, err);
out:
	return;
}

static int handle_get_verifier_req(struct pipefs_hdr *msg)
{
	int err = 0;
	struct gfs2_pnfs_get_verifier *ggv;
	struct super_block *sb;
	u32 *vin, *vout;

	ggv = payload_of(msg);
	sb = gfs2_find_sb_groupid(ggv->groupid);
	if (!sb) {
		err = -ENOENT;
		goto out;
	}

	vin = gfs2_verifier();
	vout = (u32 *)&ggv->verifier;
	*vout++ = *vin++;
	*vout = *vin;
	printk(KERN_ERR "%s: GIVING VERF %u.%u / %u.%u\n", __func__, *(vin - 1),
		*vin, *(vout - 1), *vout);
	msg->type = PNFS_GET_VERIFIER_RES;
	msg->status = 0;
out:
	return err;
}

static void handle_set_nodeid(struct pipefs_hdr *msg)
{
	struct gfs2_set_nodeid *gsn;
	struct gfs2_sbd *sbd;

	gsn = payload_of(msg);
	printk(KERN_ERR "%s: GOT NODEID %u MDS-NODEID %u GROUPID %u\n",__func__,
	       gsn->our_nodeid, gsn->mds_nodeid, gsn->groupid);

	sbd = gfs2_find_sbd_groupid(gsn->groupid);
	if (sbd) {
		sbd->our_nodeid = gsn->our_nodeid;
		sbd->mds_nodeid = gsn->mds_nodeid;
	} else
		printk(KERN_ERR "%s: ERROR: couldn't find gfs2 sbd for groupid"
			" %u\n", __func__, gsn->groupid);
}

static int alloc_init_pnfs_device(struct gfs2_ds_info *ds_info)
{
	struct gfs2_pnfs_device *gpd = NULL;

	gpd = kmalloc(sizeof(*gpd), GFP_KERNEL);
	if (!gpd)
		return -ENOMEM;

	memcpy(&gpd->ds_info, ds_info, sizeof(*ds_info));
	INIT_LIST_HEAD(&gpd->all_layouts);
	INIT_LIST_HEAD(&gpd->all_devices);

	spin_lock(&gfs2_pnfs_device_lock);
	list_add_tail(&gpd->all_devices, &gfs2_pnfs_devices);
	spin_unlock(&gfs2_pnfs_device_lock);

	return 0;
}

static void free_pnfs_device(struct gfs2_pnfs_device *gpd)
{
	spin_lock(&gfs2_pnfs_device_lock);
	list_del_init(&gpd->all_devices);
	spin_unlock(&gfs2_pnfs_device_lock);
	kfree(gpd);
}

/*
 * Does two things: first, creates a struct gfs2_pnfs_device for each DS
 * in the list.  Second, creates a comma-delimted string that will be used
 * to return DS entries from gfs2_get_device_info().  The only benefit to
 * using the string intermediary is that the ds_list can be changed at runtime
 * with the /proc interface.  I imagine this will go away at some point.
 */
static void handle_set_ds_list(struct pipefs_hdr *msg)
{
	int i, len, offset = 0, totallen = 0;
	struct gfs2_ds_list *gdl;
	char buf[XXX_PNFS_DS_LISTSZ];

	gdl = payload_of(msg);
	printk(KERN_ERR "%s: GOT DS_LIST: %d dses\n", __func__, gdl->num_dses);

	memset(buf, '\0', XXX_PNFS_DS_LISTSZ);
	for (i = 0; i < gdl->num_dses; i++) {
		printk(KERN_ERR "%s:      GOT DS: id=%u ip=|%s|\n", __func__,
			gdl->dses[i].nodeid, gdl->dses[i].ip_quad);

		len = strlen(gdl->dses[i].ip_quad);
		totallen += len + 1; /* +1 for comma */
		if (totallen >= XXX_PNFS_DS_LISTSZ) {
			printk(KERN_ERR "%s: ERROR: ds_list string will exceed "
			       "max length of %u.  NOT setting ds_list.\n",
			       __func__, XXX_PNFS_DS_LISTSZ);
			kfree(msg);
			return;
		}

		/* create the device */
		if (alloc_init_pnfs_device(&gdl->dses[i])) {
			printk(KERN_ERR "%s: ERROR: can't create device for ds "
			       "IP %s.\n", __func__, gdl->dses[i].ip_quad);
			kfree(msg);
			return;
		}

		/* record the DS IP for get_device_info */
		memcpy((buf + offset), gdl->dses[i].ip_quad, len);
		offset += len;
		if ((i + 1) < gdl->num_dses)
			buf[offset++] = ',';
	}
	strcpy(pnfs_ds_list, buf);
}

static ssize_t gfs2_pnfs_downcall(struct file *filp, const char __user *src,
				  size_t len)
{
	int err, will_reply = 0;
	struct pipefs_hdr *msg;

	if (len > PAGE_SIZE) {
		printk(KERN_ERR "%s: ERROR: msg too big (%d)\n", __func__, len);
		return -EMSGSIZE;
	}

	msg = pipefs_readmsg(filp, src, len);
	if (IS_ERR(msg)) {
		printk(KERN_ERR "%s: ERROR: couldn't pipefs_readmsg(). %ld\n",
			__func__, PTR_ERR(msg));
		return PTR_ERR(msg);
	}
	printk(KERN_ERR "%s: DOWNCALL msgid %d type %d status %d totallen %d\n",
	       __func__, msg->msgid, msg->type, msg->status, msg->totallen);

	/* for *_RES, just assign the result and wake the thread */
	if (IS_RES(msg->type)) {
		err = assign_upcall_reply(msg);
		if (err) {
			printk(KERN_ERR "%s: ERROR -- failed to assign "
				"upcall reply (msgid=%d type=%d)?\n", __func__,
				msg->msgid, msg->type);
			goto out_free;
		}
		goto out;
	}

	/* for *_REQ and others, first do whatever processing ... */
	if (msg->type == PNFS_GET_STATE_REQ) {
		err = handle_get_state_req(msg);
		if (!err)
			will_reply = 1;
	} else if (msg->type == PNFS_CHANGE_STATE_NOTIF) {
		handle_change_state_notif(msg);
	} else if (msg->type == PNFS_GET_VERIFIER_REQ) {
		err = handle_get_verifier_req(msg);
		if (!err)
			will_reply = 1;
	} else if (msg->type == PNFS_GFS2_SET_NODEID) {
		handle_set_nodeid(msg);
	} else if (msg->type == PNFS_GFS2_SET_DS_LIST) {
		handle_set_ds_list(msg);
	} else {
		printk(KERN_ERR "%s: ERROR: unknown msg type %d\n", __func__,
			msg->type);
	}

	/* ... and send off a reply (if any) */
	if (will_reply) {
		err = queue_upcall_noreply(msg, PIPEFS_AUTOFREE_UPCALL_MSG);
		if (err) {
			printk(KERN_ERR "%s: ERROR - failed w/noreply RES %d\n",
				__func__, err);
			goto out_free;
		}
	} else
		goto out_free;
out:
	return len;
out_free:
	kfree(msg);
	goto out;
}
