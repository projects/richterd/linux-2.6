/*
 *  linux/fs/gfs2/pnfs_gfs2.c
 *
 *  Copyright (c) 2008 The Regents of the University of Michigan.
 *  All rights reserved.
 *
 *  David M. Richter <richterd@citi.umich.edu>
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 *  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  With thanks to CITI's project sponsor and partner, IBM.
 */

#include "pnfs_gfs2.h"

/* following NFSD's approach in nfs4state.c */
static struct nfs4_generic_stateid zerostateid;	/* bits all 0 */
#define ZERO_STATEID(stateid) (!memcmp((stateid), &zerostateid, sizeof(zerostateid)))

spinlock_t gfs2_pnfs_device_lock;
struct list_head gfs2_pnfs_devices;
static struct kmem_cache *gfs2_layout_cachep;
static struct timeval gfs2_pnfs_boot;


u32* gfs2_verifier(void)
{
	return (u32 *)&gfs2_pnfs_boot;
}

int gfs2_init_layout_cache(void)
{
	gfs2_layout_cachep = kmem_cache_create("gfs2_pnfs_layout",
				    sizeof(struct pnfs_filelayout_layout) +
				    sizeof(struct knfsd_fh),
				    0, 0, NULL);
	if (!gfs2_layout_cachep)
		return -ENOMEM;
	return 0;
}

void gfs2_destroy_layout_cache(void)
{
	if (gfs2_layout_cachep)
		kmem_cache_destroy(gfs2_layout_cachep);
}

int gfs2_init_pnfs(void)
{
	int err;

	do_gettimeofday(&gfs2_pnfs_boot);
	spin_lock_init(&gfs2_pnfs_device_lock);
	INIT_LIST_HEAD(&gfs2_pnfs_devices);

	err = gfs2_register_sysctl();
	if (err)
		goto out;

	err = gfs2_init_layout_cache();
	if (err)
		goto out;

	err = gfs2_init_pnfs_pipe();
out:
	return err;
}

void gfs2_shutdown_pnfs(void)
{
	gfs2_shutdown_pnfs_pipe();
	gfs2_destroy_layout_cache();
	gfs2_unregister_sysctl();
}

/* we need to extract and record the mountgroup id so our mcasts work */
void gfs2_set_pnfs_groupid(struct gfs2_sbd *sdp, const char *hostdata)
{
	char *tmp1, *tmp2, *buf;

	buf = kstrdup(hostdata, GFP_KERNEL);
	if (!buf)
		goto out;

	tmp1 = strstr(buf, "id=");
	if (!tmp1)
		goto out;

	/* we don't want the journal id */
	if (tmp1 != hostdata && *(tmp1 - 1) == 'j') {
		tmp1 = strstr(++tmp1, "id=");
		if (!tmp1)
			goto out;
	}
	tmp1 += 3; // skip past "id="
	tmp2 = strchr(tmp1, ':');
	if (tmp2)
		*tmp2 = '\0';
	if (strlen(tmp1))
		sdp->groupid = (u32)simple_strtoul(tmp1, NULL, 10);
out:
	printk("%s: set PNFS GROUPID to |%u|\n", __func__, sdp->groupid);
	kfree(buf);
}


int gfs2_layout_type(struct super_block *sb)
{
	return LAYOUT_NFSV4_FILES;
}

static int get_stripe_unit(int blocksize)
{
	if (blocksize >= NFSSVC_MAXBLKSIZE)
		return blocksize;

	return NFSSVC_MAXBLKSIZE - (NFSSVC_MAXBLKSIZE % blocksize);
}

static int alloc_init_layout_info(struct inode *inode,
				  struct nfsd4_layout_seg *seg)
{
	int err = 0;
	struct gfs2_layout_info *gli;
	struct gfs2_inode *gino;
	struct gfs2_sbd *sbd;

	gli = kmalloc(sizeof(*gli), GFP_KERNEL);
	if (!gli) {
		err = -ENOMEM; /* XXX: should we really fail the lo_get here? */
		goto out;
	}
	memcpy(&gli->layout_seg, seg, sizeof(*seg));

	INIT_LIST_HEAD(&gli->per_inode);
	gino = GFS2_I(inode);
	spin_lock(&gino->layout_lock);
	list_add(&gli->per_inode, &gino->all_layouts);
	spin_unlock(&gino->layout_lock);

	INIT_LIST_HEAD(&gli->per_sb);
	sbd = GFS2_SBD(inode->i_sb);
	spin_lock(&sbd->layout_lock);
	list_add(&gli->per_sb, &sbd->all_layouts);
	spin_unlock(&sbd->layout_lock);
out:
	return err;
}

/*
 * Retrieve and encode a file layout onto the xdr stream.
 * @inode: inode for which to retrieve layout
 * @arg->xdr: xdr stream for encoding
 * @arg->func: a call into file system to encode the layout on xdr stream.
 */
int gfs2_layout_get(struct inode *inode, struct pnfs_layoutget_arg *arg)
{
	int rc = 0;
	struct pnfs_filelayout_layout *layout = NULL;
	struct gfs2_layout_info *lo_info;

	printk(KERN_DEBUG "%s: LAYOUT_GET\n", __func__);

	/* Set layout indept response args */
	arg->seg.layout_type = LAYOUT_NFSV4_FILES;
	arg->seg.offset = 0;
	arg->seg.length = inode->i_sb->s_maxbytes; /* The maximum file size */

	layout = kmem_cache_alloc(gfs2_layout_cachep, GFP_KERNEL);
	if (layout == NULL) {
		rc = -ENOMEM;
		goto error;
	}

	/* Set file layout response args */
	layout->lg_layout_type = LAYOUT_NFSV4_FILES;
	layout->lg_stripe_type = STRIPE_SPARSE;
	layout->lg_commit_through_mds = true;
	layout->lg_stripe_unit = get_stripe_unit(inode->i_sb->s_blocksize);
	layout->lg_fh_length = 1;
	layout->device_id.pnfs_fsid = arg->fsid;
	layout->device_id.pnfs_devid = 1;			/*FSFTEMP*/
	layout->lg_first_stripe_index = 0;			/*FSFTEMP*/
	layout->lg_pattern_offset = 0;
	layout->lg_fh_list = (void *)(layout + 1);

	memcpy(layout->lg_fh_list, arg->fh, sizeof(struct knfsd_fh));
	pnfs_fh_mark_ds(layout->lg_fh_list);

	/* Store information for recalling the layout */
	rc = alloc_init_layout_info(inode, &arg->seg);
	if (rc)
		goto error;

	/* Call nfsd to encode layout */
	rc = arg->func(&arg->xdr, layout);
exit:
	kmem_cache_free(gfs2_layout_cachep, layout);
	return rc;

error:
	arg->seg.length = 0;
	goto exit;
}

/*
 * Must be called with gfs2_inode->layout_lock held
 */
static struct gfs2_layout_info* find_lo_info_clientid(struct gfs2_inode *gino,
						      u64 clientid)
{
	struct gfs2_layout_info *gli = NULL;

	list_for_each_entry(gli, &gino->all_layouts, per_inode)
		if (gli->layout_seg.clientid == clientid)
			break;
	return gli;
}

int gfs2_layout_return(struct inode *inode, void *p)
{
	int err = 0;
	struct nfsd4_pnfs_layoutreturn *lr = (struct nfsd4_pnfs_layoutreturn *)p;
	struct gfs2_inode *gino = GFS2_I(inode);
	struct gfs2_layout_info *gli;

	printk(KERN_ERR "%s: LAYOUT_RETURN clid %llx lo_stid " STATEID_FMT
		"\n", __func__, lr->lr_seg.clientid, STATEID_VAL(&lr->lr_sid));

	spin_lock(&gino->layout_lock);
	gli = find_lo_info_clientid(gino, lr->lr_seg.clientid);
	if (!gli) {
		spin_unlock(&gino->layout_lock);
		printk(KERN_ERR "%s: ERROR: NO lo_info found!\n", __func__);
		return -EINVAL;
	}
	list_del(&gli->per_inode);
	spin_unlock(&gino->layout_lock);

	spin_lock(&GFS2_SBD(inode->i_sb)->layout_lock);
	list_del(&gli->per_sb);
	spin_unlock(&GFS2_SBD(inode->i_sb)->layout_lock);

	kfree(gli);
	return err;
}

void gfs2_layout_recall_file(struct dentry *dentry)
{
	int err;
	struct inode *inode = dentry->d_inode;
	struct super_block *sb = inode->i_sb;
	struct gfs2_inode *gino = GFS2_I(inode);
	struct gfs2_layout_info *gli;
	struct nfsd4_pnfs_cb_layout cbl;

	printk(KERN_ERR "%s: RECALL_FILE on |%s|\n", __func__, dentry->d_iname);
	cbl.cbl_recall_type = RECALL_FILE;

	/*
	 * XXX: need to lock the list, but the recall allocs => can sleep,
	 * so we'll need mutexes instead of spinlocks.
	 */
	list_for_each_entry(gli, &gino->all_layouts, per_inode) {
		memcpy(&cbl.cbl_seg, &gli->layout_seg, sizeof(gli->layout_seg));
		err = sb->s_export_op->cb_layout_recall(sb, inode, &cbl);
		printk(KERN_DEBUG "%s: cb_layout_recall returned %d for a "
			"recall on |%s|\n", __func__, err, dentry->d_iname);
	}
}

int gfs2_get_device_iter(struct super_block *sb, struct pnfs_deviter_arg *arg)
{
	if (arg->type != LAYOUT_NFSV4_FILES) {
		printk(KERN_ERR "%s: ERROR: layout type isn't 'file' "
			"(type: %x)\n", __func__, arg->type);
		return -ENOTSUPP;
	}

	if (arg->cookie == 0) {
		arg->cookie = 1;
		arg->verf = 1;
		arg->devid = 1;
	} else
		arg->eof = 1;

	return 0;
}

int gfs2_get_device_info(struct super_block *sb, struct pnfs_devinfo_arg *arg)
{
	int err, len, i = 0;
	struct pnfs_filelayout_device fdev;
	struct pnfs_filelayout_devaddr *daddr;
	char *ds_buf, *bufp, *bufp2;

	memset(&fdev, '\0', sizeof(fdev));
	if (arg->type != LAYOUT_NFSV4_FILES) {
		printk(KERN_ERR "%s: ERROR: layout type isn't 'file' "
			"(type: %x)\n", __func__, arg->type);
		err = -ENOTSUPP;
		goto out;
	}

	if (arg->devid.pnfs_devid != 1) {
		printk(KERN_DEBUG "%s: WARNING: didn't receive a deviceid of "
			"1 (got: 0x%llx)\n", __func__, arg->devid.pnfs_devid);
		err = -ENOENT;
		goto out;
	}

	/* XXX: no notifications yet */
	arg->notify_types = 0;

	printk(KERN_DEBUG "%s: DEBUG: current entire DS list is |%s|\n",
		__func__, pnfs_ds_list);
	if (!*pnfs_ds_list) {
		printk(KERN_ERR "%s: ERROR: pnfs_ds_list has no entries!\n",
			__func__);
		err = -EIO;
		goto out;
	}

	err = -ENOMEM;
	len = strlen(pnfs_ds_list) + 1;
	ds_buf = kmalloc(len, GFP_KERNEL);
	if (!ds_buf)
		goto out;
	memcpy(ds_buf, pnfs_ds_list, len);
	bufp = bufp2 = ds_buf;

	/* count the number of comma-delimited DS IPs */
	fdev.fl_device_length = 1;
	while ((bufp = strchr(bufp, ',')) != NULL) {
		fdev.fl_device_length++;
		bufp++;
	}

	len = sizeof(*fdev.fl_device_list) * fdev.fl_device_length;
	fdev.fl_device_list = kzalloc(len, GFP_KERNEL);
	if (!fdev.fl_device_list) {
		printk(KERN_ERR "%s: ERROR: unable to kmalloc a device list "
			"buffer for %d DSes.\n", __func__, i);
		goto out;
	}

	fdev.fl_stripeindices_length = fdev.fl_device_length;
	fdev.fl_stripeindices_list =
		kzalloc(sizeof(u32) * fdev.fl_stripeindices_length, GFP_KERNEL);

	if (!fdev.fl_stripeindices_list) {
		printk(KERN_ERR "%s: ERROR: unable to kmalloc a stripeindices "
			"list buffer for %d DSes.\n", __func__, i);
		goto out;
	}
	for (i = 0; i < fdev.fl_stripeindices_length; i++)
		fdev.fl_stripeindices_list[i] = i;

	for (i = 0; (bufp = strsep(&bufp2, ",")) != NULL; i++) {
		printk(KERN_DEBUG "%s: DEBUG: encoding DS |%s|\n", __func__,
			bufp);

		daddr = kmalloc(sizeof(*daddr), GFP_KERNEL);
		if (!daddr) {
			printk(KERN_ERR "%s: ERROR: unable to kmalloc a device "
				"addr buffer.\n", __func__);
			goto out;
		}

		daddr->r_netid.data = "tcp";
		daddr->r_netid.len = 3;
		len = strlen(bufp);
		daddr->r_addr.data = kmalloc(len + 4, GFP_KERNEL);
		memcpy(daddr->r_addr.data, bufp, len);
		/*
		 * append the port number.  interpreted as two more bytes
		 * beyond the quad: ".8.1" -> 0x08.0x01 -> 0x0801 = port 2049.
		 */
		memcpy(daddr->r_addr.data + len, ".8.1", 4);
		daddr->r_addr.len = len + 4;

		fdev.fl_device_list[i].fl_multipath_length = 1;
		fdev.fl_device_list[i].fl_multipath_list = daddr;
	}

	/* have nfsd encode the device info */
	err = arg->func(&arg->xdr, &fdev);
out:
	for (i = 0; i < fdev.fl_device_length; i++)
		kfree(fdev.fl_device_list[i].fl_multipath_list);
	kfree(fdev.fl_device_list);
	kfree(fdev.fl_stripeindices_list);
	return err;
}

int gfs2_get_state(struct inode *inode, void *fh, void *state)
{
	int err = 0;
	struct pnfs_get_state *gs;
	struct gfs2_pnfs_get_state *ggs;
	struct gfs2_sbd *sbd;
	struct pipefs_hdr *msg = NULL, *rmsg = NULL;

	sbd = GFS2_SBD(inode->i_sb);
	gs = (struct pnfs_get_state *)state;
	gs->ino = (u64)inode->i_ino;
	gs->dsid = sbd->our_nodeid;

	msg = alloc_init_msg_padded(PNFS_GET_STATE_REQ, 0, gs, sizeof(*gs),
				    sizeof(*ggs) - sizeof(*gs));
	if (!msg) {
		printk(KERN_ERR "%s: ERROR: unable to get_state\n", __func__);
		err = -ENOMEM;
		goto out;
	}
	ggs = payload_of(msg);
	ggs->groupid = sbd->groupid;

	/* set the message and queue the upcall; blocks until reply */
	rmsg = queue_upcall_waitreply(msg, 0);
	if (IS_ERR(rmsg))
		err = PTR_ERR(rmsg);

	memcpy(gs, payload_of(rmsg), sizeof(*gs));
out:
	kfree(msg);
	kfree(rmsg);
	return err;
}

void gfs2_change_state(struct file *fp)
{
	int err;
	struct gfs2_sbd *sbd;
	struct gfs2_file *gfp;
	struct gfs2_pnfs_get_state ggs;
	struct pipefs_hdr *msg = NULL;

	if (!fp || !fp->private_data || !fp->f_dentry || !fp->f_dentry->d_inode)
		return;

	sbd = GFS2_SBD(fp->f_dentry->d_inode->i_sb);
	if (sbd->mds_nodeid != sbd->our_nodeid)
		return;

	/* zero-stateid means nothing to change_state about */
	gfp = (struct gfs2_file *)fp->private_data;
	if (ZERO_STATEID(&gfp->pnfs_stateid))
		return;

	printk(KERN_ERR "%s: |%s| closed, CHANGE_STATE for " STATEID_FMT "\n",
	       __func__,fp->f_dentry->d_iname, STATEID_VAL(&gfp->pnfs_stateid));

	memcpy(&ggs.pnfs_get_state.stid, &gfp->pnfs_stateid,
		sizeof(gfp->pnfs_stateid));
	ggs.groupid = sbd->groupid;

	msg = alloc_init_msg(PNFS_CHANGE_STATE_NOTIF, 0, &ggs, sizeof(ggs));
	if (!msg) {
		printk(KERN_ERR "%s: ERROR: unable to change_state\n", __func__);
		return;
	}

	err = queue_upcall_noreply(msg, PIPEFS_AUTOFREE_UPCALL_MSG);
	if (err) {
		printk(KERN_ERR "%s: ERROR: failed w/noreply CHANGESTATE %d\n",
			__func__, err);
		kfree(msg);
	}
}

void gfs2_get_verifier(struct super_block *sb, u32 *p)
{
	int err = 0;
	u32 *rp;
	struct gfs2_sbd *sbd;
	struct gfs2_pnfs_get_verifier *ggv = NULL, *rggv = NULL;
	struct pipefs_hdr *msg = NULL, *rmsg = NULL;

	sbd = GFS2_SBD(sb);
	if (sbd->our_nodeid == sbd->mds_nodeid) {
		rp = gfs2_verifier();
		*p++ = *rp++;
		*p = *rp;
		return;
	}

	msg = alloc_init_msg_padded(PNFS_GET_VERIFIER_REQ, 0, NULL, 0,
				    sizeof(*ggv));
	if (!msg) {
		printk(KERN_ERR "%s: ERROR: unable to get_verf\n", __func__);
		err = -ENOMEM;
		goto out;
	}

	ggv = payload_of(msg);
	ggv->groupid = sbd->groupid;

	rmsg = queue_upcall_waitreply(msg, 0);
	if (IS_ERR(rmsg))
		err = PTR_ERR(rmsg);

	rggv = payload_of(rmsg);
	rp = (u32 *)&rggv->verifier;
	printk(KERN_ERR "%s: GOT VERF %u.%u\n", __func__, *rp, *(rp + 1));
	*p++ = *rp++;
	*p = *rp;
out:
	kfree(msg);
	kfree(rmsg);
}

